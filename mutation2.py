import time
import csv

f = open('test/output.csv', 'w')
writer = csv.writer(f)

result = open('test/lambda-alignment.sam', 'r').readlines()
t1 = time.time()

result = result[2:]

for l in result:
    line = l.split()
    if(line[5][0].isdigit()):
        startPosition = line[3]
        cigar = line[5]

        if len(line[9]) > 1:
            mutations = {}
            mutation = ''
            index = 0
            for simbol in cigar:
                mutation = mutation + simbol
                if simbol == 'M':
                    mutations[index] = mutation
                    index = index + 1
                    mutation = ''
                if simbol == 'D':
                    mutations[index] = mutation
                    index = index + 1
                    mutation = ''
                if simbol == 'I':
                    mutations[index] = mutation
                    index = index + 1
                    mutation = ''
                if simbol == 'S':
                    mutations[index] = mutation
                    index = index + 1
                    mutation = ''
                if simbol == 'H':
                    mutations[index] = mutation
                    index = index + 1
                    mutation = ''
            
            type = ''
            positionOrginal = int(startPosition)
            position = 0
            for index in mutations:
                number = ''
                for s in mutations[index]:
                    if s.isdigit():
                        number = number + s
                    else:
                        type = s

                if type == 'D':
                    for i in range(int(number)):
                        row = 'D,', positionOrginal, ', -'
                        writer.writerow(row)
                        #print('D,', positionOrginal, ', -')
                        positionOrginal = positionOrginal + 1
                elif type == 'I':
                    for i in range(int(number)):
                        row = 'I,', positionOrginal, ',',  line[9][position]
                        writer.writerow(row)
                        #print('I,', positionOrginal, ',',  line[9][position])
                        position = position + 1
                        positionOrginal = positionOrginal + 1
                elif type == 'M':
                    position = position + int(number)
                    positionOrginal = positionOrginal + int(number)
                elif type == 'S':
                    position = position + int(number)
                    positionOrginal = positionOrginal + int(number)
                elif type == 'H':
                    positionOrginal = positionOrginal + int(number)
                else:
                    number = ''

                #print(position)
f.close()
t2 = time.time()
time_s = t2-t1
print("Execution time: ", round(time_s, 4),"s")

