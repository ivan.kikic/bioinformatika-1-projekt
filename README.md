# Bioinformatika 1 - projekt



## Upute za implementaciju

Kako bi se indeksirali i poravnali genomi korišten je algoritam minimap2.

- [ ] [https://github.com/lh3/minimap2]

Preuzimanjem i instalacijom minimap2 algoritma dobiva se mogućnost korištenja brojnih funkcija. Funkcija -d služi za indeksiranje genoma pomoću k-mera.

```
./minimap2 -d test/ecoli.mmi test/ecoli.fasta
```

Pokretanjem ove komande pravi se binarna datoteka ecoli.mmi koja u kombinaciji sa mutiranim genomom pravi .sam datoteku koja sadržava poravnane genome.

```
./minimap2 -a test/ecoli.mmi test/ecoli_simulated_reads.fasta > ecoli-alignment.sam
```

Kada je napravljena ecoli-alignment.sam datoteka, pokretanjem python algoritma mutation2.py očitava se .sam datoteka te se u .csv datoteku ispuju sve mutacije te na kojem su se indeksu dogodile
